<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionError()
	{
		// $this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;

				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

	public function actionIndex()
	{
		$this->layout='//layouts/column1';

		$this->render('index', array(
		));
	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionPartner()
	{
		$this->pageTitle = 'Advices - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('partner', array(	
		));
	}

	public function actionAdvices()
	{
		$this->pageTitle = 'Advices - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('advices', array(	
		));
	}

	protected $n_data_choice  = [
							[
								'pict'=>'',
								'title'=>'COMPLETE BATHROOM SET',
								'desc'=>'',
							],
							[
								'pict'=>'',
								'title'=>'COMPACT BATHROOM SET',
								'desc'=>'',
							],
							[
								'pict'=>'',
								'title'=>'MINI BATHROOM SET',
								'desc'=>'',
							],
							
						  ];

	protected $n_install_data  = [
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						[
							'pict'=>'',
							'title'=>'THERMOR TYPE T01',
							'desc'=>'',
						],						
						
					  ];

	public function actionAdivices_list()
	{
		$this->pageTitle = isset($_GET['category']) ? ucwords(str_replace('-', ' ', $_GET['category'])) : "" .' Advices - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$n_data_choice = $this->n_data_choice;
		$n_install_data = $this->n_install_data;

		$this->render('advices_list', array(
			'choice_data' => $n_data_choice,
			'install_data' => $n_install_data,
		));
	}

	public function actionAdivices_detail()
	{
		$this->pageTitle = isset($_GET['category']) ? ucwords(str_replace('-', ' ', $_GET['category'])) : "" .' Advices - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('advices_detail', array(
		));
	}

	public function actionDocumentation()
	{
		$this->pageTitle = 'Documentation - ' . $this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('documentac', array(
		));
	}

	public function actionProducts_landing()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Our Solutions - '. $this->pageTitle;

		$this->render('product_landings', array(
		));		
	}

	public function actionProducts()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$data = DataProducts::resources();
		// cat
		if (isset($_GET['category'])) {
			$n_trim = trim($_GET['category']);
			$data = $data[$n_trim];
		}

		if (isset($_GET['q'])) {
			$r_searchs = [];
			foreach ($data['water_heater'] as $key => $value) {
				$r_searchs[$key] = strtolower($value['title']);
			}
			$inputs = preg_quote(htmlspecialchars($_GET['q']), '~');;
			$result = preg_grep('~' . $inputs . '~', $r_searchs);

			foreach ($result as $ke => $val) {
				$res_out[$ke] = $data['water_heater'][$ke];
			}
		}

		$this->render('product', array(
			'data' => (isset($_GET['q'])) ? $res_out : $data,
		));
	}

	public function actionProduct_detail()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$resour = DataProducts::resources();
		$n_id = intval($_GET['id']);
		$data = $resour[$_GET['category']][$n_id];
		

		$this->render('product_detail', array(
			'data'=> $data,
			'other_all' => $resour,
		));
	}

	public function actionContact()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Contact - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			$status = true;
	        $secret_key = "6LfK-q8UAAAAAIwj6e6heAAsk5eapkcNbCjZLQni";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $status = false;
	          $model->addError('verifyCode', 'Verify you are not robbot');
	        }

			if($status AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}

		$this->render('contact', array(
			'model'=>$model,
		));
	}

}


