<div class="lines-greys"></div>

<section class="back-white back-grey sections_advicesn_page">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/advices')); ?>">Our Advices</a></li>
          <?php if (isset($_GET['category']) && $_GET['category'] == 'choice-guide'): ?>
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'choice-guide')); ?>">Choice Guide</a></li>
          <?php else: ?>
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'installation-guide')); ?>">Installation Guide</a></li>
          <?php endif ?>
          <li class="breadcrumb-item active" aria-current="page"><?php echo isset($_GET['name'])? $_GET['name'] : ''; ?></li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="prelatife container">
    <div class="py-4"></div>

    
    <!-- // detail choice -->
    <?php if (isset($_GET['category']) && $_GET['category'] == 'choice-guide'): ?>
    <div class="box-detail-contents">
      <h2 class="titles text-center">CHOICE GUIDE</h2>
      <div class="py-2 my-1"></div>
      <div class="text-center">
        <h4 class="reds"><?php echo strtoupper($_GET['name']) ?></h4>
      </div>
      <div class="py-3 my-2"></div>

      <div class="description_detail text-center">
        <img src="https://placehold.it/1351x723" alt="" class="img img-fluid">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="clear clearfix"></div>
      </div>
      <div class="others_variant text-center">
        <p class="red_title">SUGGESTED PRODUCT TYPE:</p>
        <div class="py-1"></div>
        <p>THERMOR T01, THERMOR T02</p>
      </div>
      <div class="clear"></div>
    </div>      

    <?php else: ?>
    
    <!-- // installation guide -->
    <div class="box-detail-contents">
      <h2 class="titles text-center">INSTALLATION GUIDE</h2>
      <div class="py-2 my-1"></div>
      <div class="text-center">
        <h4 class="reds"><?php echo strtoupper($_GET['name']) ?></h4>
      </div>
      <div class="py-3 my-2"></div>

      <div class="description_detail text-center">
        <img src="https://placehold.it/1351x723" alt="" class="img img-fluid">
         <img src="https://placehold.it/1351x450" alt="" class="img img-fluid">
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt obcaecati maiores error, placeat quam dolorum aut excepturi earum dolores ex ad culpa eum assumenda iste cupiditate unde facere fuga exercitationem.</p>
         <img src="https://placehold.it/1351x450" alt="" class="img img-fluid">
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt obcaecati maiores error, placeat quam dolorum aut excepturi earum dolores ex ad culpa eum assumenda iste cupiditate unde facere fuga exercitationem.</p>
         <img src="https://placehold.it/1351x450" alt="" class="img img-fluid">
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt obcaecati maiores error, placeat quam dolorum aut excepturi earum dolores ex ad culpa eum assumenda iste cupiditate unde facere fuga exercitationem.</p>
         <img src="https://placehold.it/1351x450" alt="" class="img img-fluid">
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt obcaecati maiores error, placeat quam dolorum aut excepturi earum dolores ex ad culpa eum assumenda iste cupiditate unde facere fuga exercitationem.</p>
        
        <div class="clear clearfix"></div>
      </div>

      <div class="clear"></div>
    </div> 

    <?php endif ?>

    
    <!-- // parent category -->
    <div class="py-5 d-none d-sm-block"></div>
    <div class="py-4 d-block d-sm-none"></div>
    <div class="row">
      <div class="col-md-20">
        <div class="full-pict d-none d-sm-block">
          <img src="<?php echo $this->assetBaseurl ?>det-contact.jpg" alt="" class="img img-fluid">
        </div>
        <div class="full-pict d-block d-sm-none">
          <img src="<?php echo $this->assetBaseurl ?>det-contact_2.jpg" alt="" class="img img-fluid">
        </div>
      </div>
      <div class="col-md-20">
        <div class="widget-banner-def prelatife w-fullmiddle">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'choice-guide')); ?>"><img src="https://placehold.it/425x541" alt="" class="img img-fluid">
          <div class="info-default p-5">
            <h6>Choice guide</h6>
          </div>
          </a>
        </div>
      </div>
      <div class="col-md-20">
        <div class="widget-banner-def prelatife w-fullmiddle">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'installation-guide')); ?>">
          <img src="https://placehold.it/425x541" alt="" class="img img-fluid">
          <div class="info-default text-center p-5">
            <h6>Installation Guide</h6>
          </div>
          </a>
        </div>
      </div>
    </div>      

    <div class="py-4"></div>

    <div class="py-5"></div>
    <div class="clear"></div>
  </div>


  <div class="lines-greys"></div>
  <div class="py-2"></div>
  <div class="py-5 contents_2 content-text">
    <div class="prelatife container">
      <h3 class="titles_connect_contact text-center">STAY CONNECTED</h3>

      <div class="py-3"></div>
      <div class="lists_client_logo custom_row">
        <div class="row">
          <?php for ($i=1; $i < 13; $i++) { ?>
          <div class="col-md-15 col-30">
            <div class="lgo_item mb-4">
              <img src="<?php echo $this->assetBaseurl.'lgo-tk-online.jpg' ?>" alt="" class="img img-fluid w-100">
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="py-2"></div>
  
</section>

<?php echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>