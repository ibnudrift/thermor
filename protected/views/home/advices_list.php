<div class="lines-greys"></div>

<section class="back-white back-grey sections_advicesn_page">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/advices')); ?>">our Advice</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo isset($_GET['category'])? $_GET['category'] : ''; ?></li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="prelatife container">
    <div class="py-4"></div>

    <?php if (isset($_GET['category']) && $_GET['category'] == 'choice-guide'): ?>
      
    <h2 class="titles text-center">CHOICE GUIDE</h2>
    <div class="py-2 my-1"></div>
    <div class="text-center">
      <p>Please define your type of bathroom to get the best Thermor product advice</p>
    </div>
    <div class="py-3 my-2"></div>

    <div class="lists_advices_nlistdata text-center">
      <div class="row">
        <?php foreach ($choice_data as $key => $value): ?>
        <div class="col-md-20">
          <div class="items mb-3">
            <div class="pict">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_detail', 'name'=>strtolower($value['title']), 'category'=> $_GET['category'])); ?>"><img src="https://placehold.it/432x232" alt="" class="img img-fluid"></a>
            </div>
            <div class="info py-2">
               <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_detail', 'name'=>strtolower($value['title']), 'category'=> $_GET['category'])); ?>"><h5><?php echo $value['title'] ?></h5></a>
            </div>
          </div>
        </div>
        <?php endforeach ?>

      </div>
    </div>

    <?php else: ?>

    <!-- // installation guide -->
    <h2 class="titles text-center">INSTALLATION GUIDE</h2>
    <div class="py-2 my-1"></div>
    <div class="text-center">
      <p>Please define your type of bathroom to get the best Thermor product advice</p>
    </div>
    <div class="py-3 my-2"></div>

    <div class="lists_advices_nlistdata text-center">
      <div class="row">
        <?php foreach ($install_data as $key => $value): ?>
        <div class="col-md-20">
          <div class="items mb-3">
            <div class="pict">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_detail', 'name'=>strtolower($value['title']), 'category'=> $_GET['category'])); ?>"><img src="https://placehold.it/432x232" alt="" class="img img-fluid"></a>
            </div>
            <div class="info py-2">
               <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_detail', 'name'=>strtolower($value['title']), 'category'=> $_GET['category'])); ?>"><h5><?php echo $value['title'] ?></h5></a>
            </div>
          </div>
        </div>
        <?php endforeach ?>

      </div>
    </div>

    <?php endif ?>

    <div class="py-5 my-2"></div>
    <div class="row">
      <div class="col-md-20">
        <div class="full-pict d-none d-sm-block">
          <img src="<?php echo $this->assetBaseurl ?>det-contact.jpg" alt="" class="img img-fluid">
        </div>
        <div class="full-pict d-block d-sm-none">
          <img src="<?php echo $this->assetBaseurl ?>det-contact_2.jpg" alt="" class="img img-fluid">
        </div>
      </div>
      <div class="col-md-20">
        <div class="widget-banner-def prelatife w-fullmiddle">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'choice-guide')); ?>"><img src="https://placehold.it/425x541" alt="" class="img img-fluid">
          <div class="info-default p-5">
            <h6>Choice guide</h6>
          </div>
          </a>
        </div>
      </div>
      <div class="col-md-20">
        <div class="widget-banner-def prelatife w-fullmiddle">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/adivices_list', 'category'=>'installation-guide')); ?>">
          <img src="https://placehold.it/425x541" alt="" class="img img-fluid">
          <div class="info-default text-center p-5">
            <h6>Installation Guide</h6>
          </div>
          </a>
        </div>
      </div>
    </div>      

    <div class="py-4"></div>

    <div class="py-5"></div>
    <div class="clear"></div>
  </div>

  <div class="lines-greys"></div>
  <div class="py-2"></div>
  <div class="py-5 contents_2 content-text">
    <div class="prelatife container">
      <h3 class="titles_connect_contact text-center">STAY CONNECTED</h3>

      <div class="py-3"></div>
      <div class="lists_client_logo custom_row">
        <div class="row">
          <?php for ($i=1; $i < 13; $i++) { ?>
          <div class="col-md-15 col-30">
            <div class="lgo_item mb-4">
              <img src="<?php echo $this->assetBaseurl.'lgo-tk-online.jpg' ?>" alt="" class="img img-fluid w-100">
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="py-2"></div>
  
</section>

<?php echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>