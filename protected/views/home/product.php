<?php 
$p_name = [
          'water_heater' => 'ELECTRIC WATER HEATER',
          'heat_pump' => 'Domestic Heat Pump',
          'asesoris' => 'Towel Dryer',
          ];

$b_name = strtoupper($p_name[$_GET['category']]);
?>

<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'ill-about.jpg'; ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid w-100">
  </div>

  <div class="outers_breadcrumbs_cont">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo $b_name ?></li>
        </ol>
      </nav>
    </div>
  </div>

	<div class="inners_cover wow fadeInDown">		
    <div class="inners_cvr">
			<div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h2><?php echo $b_name ?></h2>
            <div class="py-1"></div>
            <p>Choose the perfect water heater for your home</p>
          </div>
        </div>
			</div>
		</div>

	</div>
</section>

<section class="solution_outer_content back-white py-5">
  <div class="py-3"></div>
  <div class="prelatife container">

  <div class="lists_electric-solution">
    <?php foreach ($data as $key => $value): ?>
    <div class="items">
      <div class="row no-gutters">
        <div class="col-md-30 my-auto">
          <div class="picts">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']), 'category'=>$_GET['category'] )); ?>">
            <img src="<?php echo $this->assetBaseurl .'product/'. $value['folder'].'/'. $value['picture'][0] ?>" alt="" class="img-fluid">
            </a>
          </div>
        </div>
        <div class="col-md-30 grey p-4">
          <div class="descriptions py-3 px-4">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']), 'category'=>$_GET['category'] )); ?>"><h3><?php echo $value['title'] ?></h3></a>
            <div class="py-1"></div>
            <p><?php echo $value['intro_desc'] ?></p>
            <div class="py-3"></div>
            <div class="lists_small">
              <div class="row">
                <?php foreach ($value['cvr_icon'] as $kenl => $vanl): ?>
                <div class="col-md-20 col-20">
                  <div class="texts mb-3">
                    <span><?php echo $vanl['name'] ?></span>
                    <div class="py-1"></div>
                    <?php echo $vanl['values'] ?>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="py-4"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']), 'category'=>$_GET['category'] )); ?>" class="btn btns_custom_def bck-full d-block mx-auto">Learn more</a>
          </div>
          <!-- end descriptions -->
        </div>
      </div>
    </div>
    <div class="py-4 my-2"></div>
    <!-- end items -->
    <?php endforeach; ?>

    <div class="clear"></div>
  </div>



  </div>
  
</section>
