
<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'slide-3.jpg'; ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid w-100">
  </div>
  <div class="outers_breadcrumbs_cont">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
      </nav>
    </div>
  </div>
	<div class="inners_cover wow fadeInDown">		
    <div class="inners_cvr">
			<div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h2>THERMOR</h2>
            <div class="py-1"></div>
            <p>Your Expert Since 1931</p>
          </div>
        </div>
			</div>
		</div>

	</div>
</section>

<section class="about_outer_content back-white py-5">
  
  <div class="tops-about lines_btm">
    <div class="prelatife container">
      <div class="txt">
        <div class="row">
          <div class="col-md-20 col-20">
            <div class="texts">
              <a class="toscroll" data-id="our-story" href="#">OUR STORY</a>
            </div>
          </div>
          <div class="col-md-20 col-20">
            <div class="texts">
              <a class="toscroll" data-id="our-value" href="#">OUR VALUE</a>
            </div>
          </div>
          <div class="col-md-20 col-20">
            <div class="texts">
              <a class="toscroll" data-id="our-product" href="#">THERMOR</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="prelatife container">
    <div class="py-4 my-1"></div>
    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-40">
          <h2>Thermor believes in bringing innovation <br><b>for everyone’s comfort</b></h2>
        </div>
        <div class="col-md-10"></div>
      </div>
      <div class="py-5"></div>

      <div class="boxs-inner-about" id="our-story">
        <h5>OUR STORY</h5>
        <div class="py-2"></div>
        <p><b>PT. Thermor Perwira Ardha</b> diberikan kepercayaan oleh Atlantic Group sebagai distributor tunggal pemanas air Thermor di Indonesia. Dengan tenaga ahli yang berpengalaman luas di bidang pemanas air, PT. Thermor Perwira Ardha ingin menjadikan Thermor sebagai solusi terbaik untuk kebutuhan pemanas air.</p>
        <p>Mengikuti perkembangan teknologi dan pasar pemanas air, PT. Thermor Perwira Ardha akan selalu mengutamakan inovasi serta memberikan pelayanan yang terbaik bagi seluruh masyarakat Indonesia.</p>
        <img src="<?php echo $this->assetBaseurl.'banner-about-1.jpg'; ?>" alt="" class="img img-fluid">
      </div>

      <div class="py-4"></div>

      <div class="boxs-inner-about text-left" id="our-value">
        <h5 class="text-center">OUR VALUE</h5>
        <div class="py-2"></div>

        <div class="row">
          <div class="col-md-60 text-center">
            <h3 class="sub-titles">Thermor, pemanas air terbaik dari Perancis</h3>
            <p>Thermor adalah pelopor dan brand ternama dari Perancis dengan sejarah panjang dalam pasar pemanas listrik dan sistem pemanas air. Sekarang tersedia di lebih dari 70 negara, Thermor selalu mengutamakan inovasi, teknologi terkini, dan desain yang modern. Dengan pengalaman lebih dari 80 tahun, Thermor menjadi ahli dalam pemanas air dari segi kekuatan, fungsi, dan desain.</p>
            
            <div class="py-2"></div>
            <h3 class="sub-titles">Thermor adalah bagian dari Atlantic Group:</h3>
            <p>Atlantic Group adalah perusahaan Perancis no. 1 yang bergerak di bidang teknologi pemanas air dan udara.</p>

            <div class="py-2"></div>
            <h3 class="sub-titles">Atlantic Group telah berdiri sejak tahun 1968, merupakan:</h3>
            <div class="py-2"></div>

            <div class="bxn_custom_thermor">
              <div class="row no-gutters justify-content-center">
                  <div class="col-md-20 my-auto">
                    <div class="texts d-block mx-auto text-center">
                      <p>Pemain utama di bidang penghangat air & udara</p>
                    </div>
                  </div>
                  <div class="col-md-20 my-auto">
                    <div class="texts d-block mx-auto text-center">
                      <p>7,100 pegawai, termasuk 3200 di luar Perancis</p>
                    </div>
                  </div>
                  <div class="col-md-20 my-auto">
                    <div class="texts d-block mx-auto text-center">
                      <p>1.66 M€ penjualan</p>
                    </div>
                  </div>
              </div>
              <div class="py-2"></div>
              <div class="lines-grey"></div>
              <div class="py-2"></div>
              <div class="row no-gutters justify-content-center">
                  <div class="col-md-30 my-auto">
                    <div class="texts d-block mx-auto text-center">
                      <p>23 pabrik di seluruh dunia: 11 di Perancis dan 12 di beberapa negara</p>
                    </div>
                  </div>
                  <div class="col-md-30 my-auto">
                    <div class="texts d-block mx-auto text-center">
                      <p>4% dari penjualan dialokasikan ke Penelitian & Pengembangan</p>
                    </div>
                  </div>
              </div>
            </div>
            <div class="py-3 my-1"></div>
            <div class="clear clearfix"></div>
          </div>

          <div class="clear"></div>
        </div>

        <div class="clear"></div>
      </div>
  
      <div class="py-4"></div>

      <div class="boxs-inner-about" id="our-product">
        <h5>TEKNOLOGI PEMANAS AIR THERMOR</h5>
        <div class="py-2"></div>
        <p>Thermor telah mengembangkan inovasi dan teknologi terbaik demi mencapai kualitas produk yang optimal, kenyamanan bagi pengguna dan penghematan energy.</p>
        <div class="py-4"></div>
      </div>

      <div class="clear clearfix"></div>
    </div>

    <div class="clear"></div>
  </div>
</section>

<?php // echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>