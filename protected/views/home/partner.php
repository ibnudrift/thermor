<div class="lines-greys"></div>

<section class="contacts_outer_content back-grey">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Our Partner</li>
        </ol>
      </nav>
    </div>
  </div>
  
</section>
<!-- <div class="lines-greys"></div> -->
<section class="middles_contact bg-white">

  <div class="py-5 contents_1 partners_loc content-text bg-white">
    <div class="prelatife container py-5 my-3">
    <h5 class="text-center">WE HAVE PARTNERS AROUND THE NATION TO HELP PROVIDE <br>
    THERMOR WATER HEATER TO YOU, BROWSE THE LOCATION BELOW</h5>
    <div class="py-3 my-1"></div>
    <div class="row">
      <div class="col-md-35 mx-auto">
        <?php 
        $query = Yii::app()->db->createCommand("SELECT `kota` FROM store_address GROUP BY kota")->queryAll();
        ?>
        <form action="" class="set_check_form">
          <select name="city" id="" class="form-control changed_city">
            <option value="">Select City</option>
            <?php foreach ($query as $key => $value): ?>
            <option value="<?php echo $value['kota'] ?>"><?php echo ucwords(str_replace('-', ' ', $value['kota'])); ?></option>
            <?php endforeach ?>
          </select>
        </form>
      </div>

      <?php if (isset($_GET['city']) && $_GET['city'] != ''): ?>
        <?php 
        $data = StoreAddress::model()->findAll('kota = :kotas', array(':kotas'=> trim($_GET['city']) ));
        ?>
        <div class="col-md-60">

        <div class="py-5"></div>
        <div class="rights_info blocks_info_address">
          <h3 class="titles-city"><?php echo ucwords($_GET['city']) ?></h3>
          <div class="py-1"></div>

          <div class="blocks_address">
            <div class="row justify-content-center">
              <?php foreach ($data as $key => $value): ?>
                <div class="col-md-20">
                  <div class="lists text-center mb-4 pb-3">
                    <p><b><?php echo $value->nama_toko ?></b><br>
                      <?php echo $value->phone; ?><br>
                      <?php echo $value->address ?>
                    </p>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>

          <div class="clear clearfix"></div>
        </div>

        </div>
      <?php endif ?>

        </div>
        <!-- end row -->

    </div>
  </div>
  <script type="text/javascript">
    $(function(){
      
      $('.changed_city').change(function(){
        var sn_val = $(this).val();
        $('.set_check_form').submit();
      });

    });
  </script>

</section>
<style type="text/css">
.content-text h5, 
.content-text h6{
  color: #58595b;
}  
</style>

<?php // echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>