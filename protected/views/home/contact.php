<div class="lines-greys"></div>

<section class="contacts_outer_content back-grey">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="prelatife container">
    <div class="py-4"></div>

    <div class="content-text">
      <h2>WE`RE HERE FOR YOU</h2>
      <div class="py-2f"></div>
      <div class="row">
        <div class="col-md-35 bloc_desc_contact">
          <div class="py-3"></div>
          <!-- <h2 class="contact_hotline">
            HOTLINE: <br>
            <b>1500-THERMOR</b>
          </h2>
          <div class="py-3"></div> -->
          <h2 class="contact_hotline">HEAD OFFICE:</h2>
          <p>Jl. Pattimura, Plaza Segi 8 blok A837<br>Surabaya 60189<br>(031) 99148641</p>
        </div>
        <div class="col-md-25">

          <!-- formcontact -->
          <?php echo $this->renderPartial('//home/_form_contact2', array('model'=>$model) ); ?>
          <!-- end form contact -->

          <div class="clear"></div>
        </div>
      </div>
    </div>

    <div class="py-5"></div>
    <div class="py-1"></div>
    <div class="clear"></div>
  </div>
  
</section>

<?php /*
<section class="middles_contact back-white">

  <div class="py-5 contents_1 partners_loc content-text">
    <div class="prelatife container py-5 my-3">
    <h5 class="text-center">FIND THERMOR WATER HEATER NEAR YOU</h5>
    <div class="py-3 my-1"></div>
    <div class="row">
      <div class="col-md-35 mx-auto">
        <?php 
        $query = Yii::app()->db->createCommand("SELECT `kota` FROM store_address GROUP BY kota")->queryAll();
        ?>
        <form action="" class="set_check_form">
          <select name="city" id="" class="form-control changed_city">
            <option value="">Select City</option>
            <?php foreach ($query as $key => $value): ?>
            <option value="<?php echo $value['kota'] ?>"><?php echo ucwords(str_replace('-', ' ', $value['kota'])); ?></option>
            <?php endforeach ?>
          </select>
        </form>
      </div>

      <?php if (isset($_GET['city']) && $_GET['city'] != ''): ?>
        <?php 
        $data = StoreAddress::model()->findAll('kota = :kotas', array(':kotas'=> trim($_GET['city']) ));
        ?>
        <div class="col-md-60">

        <div class="py-5"></div>
        <div class="rights_info blocks_info_address">
          <h3 class="titles-city"><?php echo ucwords($_GET['city']) ?></h3>
          <div class="py-1"></div>

          <div class="blocks_address">
            <div class="row justify-content-center">
              <?php foreach ($data as $key => $value): ?>
                <div class="col-md-20">
                  <div class="lists text-center mb-4 pb-3">
                    <p><b><?php echo $value->nama_toko ?></b><br>
                      <?php echo $value->phone; ?><br>
                      <?php echo $value->address ?>
                    </p>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>

          <div class="clear clearfix"></div>
        </div>

        </div>
      <?php endif ?>

        </div>
        <!-- end row -->

    </div>
  </div>
  <script type="text/javascript">
    $(function(){
      
      $('.changed_city').change(function(){
        var sn_val = $(this).val();
        $('.set_check_form').submit();
      });

    });
  </script>
  <!-- <div class="lines-greys"></div> -->

  <div class="py-5 contents_2 content-text d-none">
  <div class="py-2"></div>
    <div class="prelatife container">
      <h3 class="titles_connect_contact text-center">STAY CONNECTED</h3>

      <div class="lists_client_logo custom_row hide hidden">
      <div class="py-3"></div>
        <div class="row">
          <?php for ($i=1; $i < 13; $i++) { ?>
          <div class="col-md-15 col-30">
            <div class="lgo_item mb-4">
              <img src="<?php echo $this->assetBaseurl.'lgo-tk-online.jpg' ?>" alt="" class="img img-fluid w-100">
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      
    </div>
  </div>

</section>
*/ ?>

<?php // echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>