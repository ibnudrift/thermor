<div class="lines-greys"></div>

<section class="back-white back-grey sections_advicesn_page">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Documentation</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="prelatife container text-center">
    <div class="py-4"></div>

    <h1 class="bg-titles">FIND THE RIGHT DOCUMENTATION<br>FOR YOUR THERMOR PRODUCT</h1>
    <div class="py-4 my-1"></div>

    <h2 class="titles text-center">TECHNICAL SHEET</h2>
    <div class="py-3 my-2"></div>
    <div class="lists_doc_sheets">
      <div class="row">
        <?php for ($i=1; $i < 5; $i++) { ?>
        <div class="col-md-15">
          <div class="items mb-4">
            <div class="pict mx-auto text-center"><img src="https://placehold.it/134x134" alt="" class="img img-fluid mx-auto d-block"></div>
            <div class="info py-3">
              <h5>THERMOR T0<?php echo $i ?><br>TECHNICAL SHEET</h5>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="py-4"></div>

    <h2 class="titles text-center">USER MANUAL</h2>
    <div class="py-3 my-2"></div>
    <div class="lists_doc_sheets">
      <div class="row">
        <?php for ($i=1; $i < 5; $i++) { ?>
        <div class="col-md-15">
          <div class="items mb-4">
            <div class="pict mx-auto text-center"><img src="https://placehold.it/134x134" alt="" class="img img-fluid mx-auto d-block"></div>
            <div class="info py-3">
              <h5>THERMOR T0<?php echo $i ?><br>TECHNICAL SHEET</h5>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="py-4 my-2"></div>
    <div class="clear"></div>
  </div>


  <div class="lines-greys"></div>
  <div class="py-2"></div>
  <div class="py-5 contents_2 content-text">
    <div class="prelatife container">
      <h3 class="titles_connect_contact text-center">STAY CONNECTED</h3>

      <div class="py-3"></div>
      <div class="lists_client_logo custom_row">
        <div class="row">
          <?php for ($i=1; $i < 13; $i++) { ?>
          <div class="col-md-15 col-30">
            <div class="lgo_item mb-4">
              <img src="<?php echo $this->assetBaseurl.'lgo-tk-online.jpg' ?>" alt="" class="img img-fluid w-100">
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="py-2"></div>
  
</section>

<?php echo $this->renderPartial('//layouts/_lay_btm_gallery', array()); ?>