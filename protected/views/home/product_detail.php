
<?php 
$p_name = [
          'water_heater' => 'ELECTRIC WATER HEATER',
          'heat_pump' => 'Domestic Heat Pump',
          'asesoris' => 'Towel Dryer',
          ];

$b_name = strtoupper($p_name[$_GET['category']]);
?>

<div class="lines-greys"></div>
<section class="solution_outer_content backs_detailprd_1 back-grey">
  <div class="py-2"></div>
  <div class="outers_breadcrumbs_cont layout2">
    <div class="prelatife container">
      
      <div class="row">
        <div class="col-md-30">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb p-0">
              <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><?php echo $b_name ?></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo ucwords(strtolower($data['title'])) ?></li>
            </ol>
          </nav>
        </div>
        <div class="col-md-30">
          <div class="text-right backs_to_categorys">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=> $_GET['category'])); ?>" class="btn btn-link"><i class="fa fa-chevron-left"></i> BACK</a>
          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="prelatife container">

  <div class="py-3"></div>

  <div class="box_detail_electric">
      <div class="row no-gutters">
        <div class="col-md-30">
          <div class="picts">
            <div id="n_carousel" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php foreach ($data['picture'] as $key => $value): ?>
                <div class="carousel-item <?php echo ($key == 0)? 'active':'' ?>">
                  <img class="d-block w-100" src="<?php echo $this->assetBaseurl.'product/'.$data['folder'].'/'.$value ?>" alt="">
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <!-- End carousel -->
          </div>
          <div class="clear clearfix"></div>
          <div class="pict_small mt-3">
            <ul class="list-inline others_lst_pictures">
              <?php foreach ($data['picture'] as $key => $value): ?>
              <li class="list-inline-item">
                <a href="#" data-id="<?php echo $key ?>"><img src="<?php echo $this->assetBaseurl.'product/'.$data['folder'].'/'.$value ?>" alt="" class="img img-fluid"></a>
              </li>
              <?php endforeach ?>
            </ul>
          </div>
          <script type="text/javascript">
            $(function(){
              
              $('ul.others_lst_pictures li a').on('click', function(e){
                var n_id = parseInt( $(this).attr('data-id') );
                $('#n_carousel').carousel(n_id);
                e.preventDefault();
              });

            });  
          </script>

        </div>
        <div class="col-md-30 grey px-4">
          <div class="descriptions px-4">
            <h3><?php echo $data['title'] ?></h3>
            <div class="py-1"></div>
            <p><?php echo $data['intro_desc'] ?></p>
            <?php if ($data['description']): ?>
              <div class="py-2"></div>
              <?php echo $data['description'] ?>
            <?php endif ?>
            <div class="py-3"></div>

            <?php if ($data['additional_icon']): ?>
            <div class="lists_small">
              <div class="row">
                <?php foreach ($data['additional_icon'] as $key => $value): ?>
                <div class="col-md-60">
                  <div class="texts mb-3 pb-2">
                    <div class="row">
                      <div class="col-md-10">
                        <img src="<?php echo $this->assetBaseurl. 'product/'.$data['folder'].'/'. $value['icons'] ?>" alt="" class="img img-fluid">
                      </div>
                      <div class="col my-auto">
                        <p class="mb-0"><?php echo $value['text'] ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>
              </div>
            </div>
            <?php endif; ?>

            <div class="py-2"></div>
            <!-- <a href="#" class="btn btns_custom_def bck-full d-block mx-auto">Download technical sheet</a>
            <div class="py-2"></div> -->
            <?php if ($data['manual']): ?>
            <a target="_blank" href="<?php echo $this->assetBaseurl. 'product/'. $data['manual'] ?>" class="btn btns_custom_def bck-full d-block mx-auto">Download user manual</a>
            <?php endif ?>
          </div>
          <!-- end descriptions -->
        </div>
    </div>
  </div>

  <div class="py-3"></div>

  </div>
  
</section>

<section class="electric-details-2 back-white">
  <div class="prelatife container">

    <?php if ($data['why_list_data']): ?>
    <div class="py-4"></div>
    <div class="py-2"></div>
    <div class="content-text">
      <h2>WHY CHOOSE <?php echo $data['title'] ?>?</h2>
      <div class="py-2"></div>
      <div class="blocks_sectionproduct_blok2">
        <div class="row">
          <div class="col-md-45">
              <div class="lefts_block_icons">
                <div class="row no-gutters">
                  <?php foreach ($data['why_list_data'] as $key => $value): ?>
                  <div class="col-md-20 my-auto <?php echo ($key < 3) ? 'tops_dt':''; ?>">
                    <div class="boxs_item text-center">
                      <?php if ($_GET['category'] != 'water_heater'): ?>
                        <div class="pict"><img src="<?php echo $this->assetBaseurl .'product/'.$data['folder'].'/'. $value['icons'] ?>" alt="" class="img img-fluid"></div>
                      <?php else: ?>
                        <div class="pict"><img src="<?php echo $this->assetBaseurl .'product'.'/'. $value['icons'] ?>" alt="" class="img img-fluid"></div>
                      <?php endif; ?>
                      <div class="info">
                        <h6><?php echo $value['text'] ?></h6>
                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>
                </div>
                <!-- End left block -->
              </div>            
          </div>
          <div class="col-md-15">
              <div class="b_rights_benefits">
                <h5><?php echo $data['benefit_content_title'] ?></h5>
                <div class="py-1"></div>
                <?php echo $data['benefit_content'] ?>
                <div class="clear"></div>
              </div>
          </div>
        </div>

      </div>
      <div class="clear"></div>
    <?php endif ?>
    <div class="py-5"></div>

      <div class="blocks_sectionproduct_blok3 bg-white">
          <div class="block_grey">
            <div class="row">
              <div class="col-md-30">
                <div class="banners">
                  <?php if ($data['folder'] == 'ristretto' || $data['folder'] == 'concept' || $data['folder'] == 'airlis' || $data['folder'] == 'towel'): ?>
                    <img src="<?php echo $this->assetBaseurl ?>product/<?php echo $data['folder'] ?>/<?php echo $data['spec_pictures']; ?>" alt="" class="img img-fluid d-block mx-auto">
                  <?php else: ?>
                    <img src="<?php echo $this->assetBaseurl ?>product/<?php echo $data['folder'] ?>/<?php echo substr($data['picture'][2], 0, -3).'png' ?>" alt="" class="img img-fluid d-block mx-auto">
                  <?php endif ?>
                </div>
              </div>
              <div class="col-md-30 my-auto">
                <div class="infos">
                  <?php echo $data['spec_content'] ?>
                </div>
              </div>
            </div>
          </div>

          <div class="py-4"></div>

        <div class="block_grey p-4">
          <h5>DIMENSI INSTALASI (mm)</h5>
          <div class="py-2"></div>
          <div class="banners_pict p-4 px-5"><img src="<?php echo $this->assetBaseurl.'product/'.$data['folder'] .'/'. $data['data_dimensi_instalasi'] ?>" alt="" class="img img-fluid d-block mx-auto"></div>
        </div>

        <!-- end row -->
        <?php if ($data['data_dimensi']): ?>
        <div class="py-4"></div>
        <div class="row">
          <div class="col-md-60">
            <div class="block_grey p-4">
              <h5>DIMENSI</h5>
              <div class="py-2"></div>
              <?php if ($data['folder'] == 'ristretto' || $data['folder'] == 'concept' || $data['folder'] == 'airlis'): ?>
                <p style="max-width: 70%;" class="d-block mx-auto"><img src="<?php echo $this->assetBaseurl.'product'.'/'. $data['folder'].'/'. $data['data_dimensi'] ?>" alt="" class="img img-fluid d-block mx-auto"></p>
              <?php else: ?>
              <div class="table-responsive">
                <?php echo $data['data_dimensi'] ?>
              </div>
              <?php endif; ?>

              <div class="clear"></div>
            </div>
          </div>
        </div>
        <?php endif ?>

        <div class="py-4"></div>
        <div class="row">
          <div class="col-md-60">
            <div class="block_grey p-4">
              <h5>DATA TEKNIKAL</h5>
              <div class="py-2"></div>
              <?php if ($data['folder'] == 'ristretto' || $data['folder'] == 'concept' || $data['folder'] == 'airlis' || $data['folder'] == 'towel'): ?>
                <p style="max-width: 70%;" class="d-block mx-auto"><img src="<?php echo $this->assetBaseurl.'product'.'/'. $data['folder'].'/'. $data['data_teknikal'] ?>" alt="" class="img img-fluid d-block mx-auto"></p>
              <?php else: ?>
              <div class="table-responsive">
                <?php echo $data['data_teknikal'] ?>
              </div>
              <?php endif; ?>
              <div class="clear"></div>
            </div>
          </div>
        </div>

        <div class="clear"></div>
      </div>
      

      <div class="clear"></div>
    </div>

    <div class="py-3"></div>
    <div class="py-4"></div>
    <div class="clear clearfix"></div>
  </div>
</section>

<script type="text/javascript">
  $(function(){
    $('.table-responsive').find('table').addClass('table');
  })
</script>

<style type="text/css">
  .box_detail_electric .descriptions p, .lists_electric-solution .descriptions p{
    font-size: 1.35rem;
  }
</style>

<?php // echo $this->renderPartial('//layouts/_lay_btm_gallery', array('other_all' => $other_all)); ?>