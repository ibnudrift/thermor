<!-- start form c -->
<div class="box-form tl-contact-form">
  <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                  'type'=>'',
                  'enableAjaxValidation'=>false,
                  'clientOptions'=>array(
                      'validateOnSubmit'=>false,
                  ),
                  'htmlOptions' => array(
                      'enctype' => 'multipart/form-data',
                  ),
              )); ?>
   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <?php $this->widget('bootstrap.widgets.TbAlert', array(
            'alerts'=>array('success'),
        )); ?>
    <?php endif; ?>

    <div class="form-group">
        <label class="sr-only">Name</label>
        <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'required'=>'required', 'placeholder'=> 'name')); ?>
    </div>
        
    <div class="form-group">
        <label class="sr-only">Phone</label>
        <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'required'=>'required', 'placeholder'=> 'phone number')); ?>
    </div>

    <div class="form-group">
        <label class="sr-only">Email</label>
        <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'required'=>'required', 'placeholder'=> 'email')); ?>
    </div>

    <div class="form-group">
        <label class="sr-only">Message</label> 
        <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>7, 'placeholder'=>'Message')); ?>
    </div>
    
    <div class="form-group">
        <div class="g-recaptcha" data-sitekey="6LfK-q8UAAAAAKi1Sos20Tv0yFdxcb-Eykz35_Ci"></div>
    </div>
    
    <button type="submit" class="btn btn-default btns-submit-bt">SUBMIT</button>
  <?php $this->endWidget(); ?>
</div>
<!-- end form c -->
<script src='https://www.google.com/recaptcha/api.js'></script>