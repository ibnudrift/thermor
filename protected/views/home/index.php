<section class="home_tops_red py-4">
	<div class="prelatife container py-2">
		<div class="row">
			<div class="col-md-45">
				<h4>THEMOR: YOUR EXPERT SINCE 1931</h4>
				<p>Over 82 years experience has made Thermor one of the largest <br>international brands in the electric heating and hot water sector.</p>
			</div>
			<div class="col-md-15 text-right">
				<a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
			</div>
		</div>
		<div class="clear clearfix"></div>
	</div>
</section>

<section class="home-section-1 py-5">
    <div class="prelatife container">
        <div class="inners py-3">
            
            <h2 class="titles text-center">TAILOR-MADE SOLUTIONS</h2>
            <div class="py-3"></div>
            
            <div class="widget-banner-def prelatife">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'water_heater')); ?>">
                  	<img src="<?php echo $this->assetBaseurl.'home_banners_1.jpg' ?>" alt="" class="img img-fluid">
                  	<div class="info-default p-5">
                  		<h6>Electric Water Heater Solutions</h6>
                  	</div>
                  </a>
            </div>

            <div class="py-3"></div>
            <div class="row">
            	<div class="col-md-20">
            		<div class="widget-banner-def prelatife">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'asesoris')); ?>">
	            	<img src="<?php echo $this->assetBaseurl ?>towel-cvr-homes.jpg" alt="" class="img img-fluid">
	            	<div class="info-default p-5">
	            		<h6>Towel <br>Dryer</h6>
	            	</div>
                        </a>
		            </div>
            	</div>
            	<div class="col-md-40">
            		<div class="widget-banner-def prelatife">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'heat_pump')); ?>">
      	            	<img src="<?php echo $this->assetBaseurl.'home-banners-s2.jpg' ?>" alt="" class="img img-fluid">
      	            	<div class="info-default p-5">
      	            		<h6>Renewable energy solutions</h6>
      	            	</div>
                        </a>
		            </div>
            	</div>
            </div>

            <div class="d-none d-sm-block">
                  <div class="py-4 my-2"></div>
            </div>
            <div class="d-block d-sm-none">
                  <div class="py-3"></div>
            </div>

            <div class="d-none">
                  <h2 class="titles text-center">OUR ADVICES FOR YOU</h2>
                  <div class="py-3"></div>
                  <div class="row">
                  	<div class="col-md-20">
                  		<div class="full-pict"><img src="<?php echo $this->assetBaseurl ?>det-contact.jpg" alt="" class="img img-fluid"></div>
                  	</div>
                  	<div class="col-md-20">
                  		<div class="widget-banner-def prelatife w-fullmiddle">
      		            	<img src="https://placehold.it/425x541" alt="" class="img img-fluid">
      		            	<div class="info-default p-5">
      		            		<h6>Choice guide</h6>
      		            	</div>
      		            </div>
                  	</div>
                  	<div class="col-md-20">
                  		<div class="widget-banner-def prelatife w-fullmiddle">
      		            	<img src="https://placehold.it/425x541" alt="" class="img img-fluid">
      		            	<div class="info-default text-center p-5">
      		            		<h6>Installation Guide</h6>
      		            	</div>
      		            </div>
                  	</div>
                  </div>      
            </div>


            <?php 
            /*
            <h2 class="titles text-center">STAY CONNECTED</h2>
            <div class="py-3"></div>
            <div class="lists_client_logo custom_row">
            	<div class="row">
            		<?php for ($i=1; $i < 13; $i++) { ?>
	            	<div class="col-md-15 col-30">
	            		<div class="lgo_item mb-4">
	            			<img src="<?php echo $this->assetBaseurl.'lgo-tk-online.jpg' ?>" alt="" class="img img-fluid w-100">
	            		</div>
	            	</div>
            		<?php } ?>
            	</div>
            </div>

            <div class="py-2"></div>
            */
            ?>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>
