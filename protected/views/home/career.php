<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['career_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['career_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['career_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="career_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3"></div>

    <div class="content-text text-center">
      <div class="row">
        <div class="col-3"></div>
        <div class="col-54">
          <?php echo $this->setting['career_content'] ?>
        </div>
        <div class="col-3"></div>
      </div>

      <div class="py-4"></div>

      <?php 
      $nx_data = Career::model()->findAll();
      ?>

      <div class="outers_lists_career text-left">
        <?php foreach ($nx_data as $key => $value): ?>
        <div class="row">
          <div class="col-md-14">
            <h5><?php echo $value->position ?></h5>
            <p>Work Locations: <br>
            <b><?php echo $value->location ?></b></p>
            <p>Email to apply for this job: <br>
            <a href="mailto:career@ifura.com">career@ifura.com</a></p>
          </div>
          <div class="col-md-23">
            <?php if (Yii::app()->language == 'en'): ?>
            <div class="sub-title">
              <h4 class="sub">job description</h4>
            </div>
              <?php echo $value->desc_en; ?>
            <?php else: ?>
              <div class="sub-title">
                <h4 class="sub">deskripsi pekerjaan</h4>
              </div>
              <?php echo $value->desc_id; ?>
            <?php endif ?>
          </div>
          <div class="col-md-23">
              <?php if (Yii::app()->language == 'en'): ?>
            <div class="sub-title">
              <h4 class="sub">qualifications</h4>
            </div>
              <?php echo $value->kualifikasi_en; ?>
            <?php else: ?>
              <div class="sub-title">
              <h4 class="sub">kualifikasi</h4>
            </div>
              <?php echo $value->kualifikasi_id; ?>
            <?php endif ?>
          </div>
        </div>

        <div class="py-3"></div>
        <div class="lines-grey"></div>
        <div class="py-3"></div>
        <?php endforeach ?>

      </div>
      <!-- End outer list career -->
      
      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>
