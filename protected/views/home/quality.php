<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['quality_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['quality_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['quality_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="quality_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3 d-none d-sm-block"></div>

    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['quality1_content'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="py-4"></div>

      <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality1_pictures'] ?>" alt="">

      <div class="py-5"></div>
      <div class="py-5"></div>

      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>


<section class="quality_outer_content2 back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    
    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
            <!-- cstm -->
            <?php echo $this->setting['quality2_content'] ?>
            <div class="clear"></div>          
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="py-3"></div>

      <?php 
        $list_achieve = [
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img1'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title1'],
                          ],
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img2'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title2'],
                          ],
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img3'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title3'],
                          ],
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img4'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title4'],
                          ],
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img5'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title5'],
                          ],
                          [
                            'picture'=> Yii::app()->baseUrl.ImageHelper::thumb(350,343, '/images/static/'. $this->setting['quality2_banner_img6'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                            'names'=> $this->setting['quality2_banner_title6'],
                          ],
                          
                        ];
      ?>
      <div class="lists_achieve_perfection">
        <div class="row">
          <?php foreach ($list_achieve as $key => $value): ?>
          <div class="col-md-20">
            <div class="boxed prelatife">
              <div class="block-name"><span class="name-tag"><?php echo $value['names'] ?></span></div>
              <img src="<?php echo $value['picture']; ?>" alt="" class="img img-fluid">
            </div>
          </div>
          <?php endforeach ?>
        </div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>


<section class="quality_outer_content3 back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    
    <div class="content-text text-center">
      <h2><strong><?php echo $this->setting['quality4_title'] ?></strong></h2>
      <div class="py-3"></div>
    
    <div class="lists_icon_commitment">
     <div class="row">

       <div class="col">
         <div class="boxed">
           <div class="pic pb-1"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(120,119, '/images/static/'. $this->setting['quality4_banner_img1'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
           <div class="info py-3">
             <p><?php echo $this->setting['quality4_banner_title1'] ?></p>
           </div>
         </div>
       </div>
       <div class="col">
         <div class="boxed">
           <div class="pic pb-1"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(120,119, '/images/static/'. $this->setting['quality4_banner_img2'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
           <div class="info py-3">
             <p><?php echo $this->setting['quality4_banner_title2'] ?></p>
           </div>
         </div>
       </div>
       <div class="col">
         <div class="boxed">
           <div class="pic pb-1"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(120,119, '/images/static/'. $this->setting['quality4_banner_img3'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
           <div class="info py-3">
             <p><?php echo $this->setting['quality4_banner_title3'] ?></p>
           </div>
         </div>
       </div>
       
       
     </div>
     </div>


      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>


<section class="quality_outer_content4 back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    
    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['quality3_content'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="py-3"></div>
    
    <div class="lists_icon_certificate">
       <div class="row">
        <?php for ($i=1; $i <= 6; $i++) { ?>
           <div class="col">
             <div class="boxed">
               <div class="pic pb-1"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality3_banner_img'. $i] ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid"></div>
             </div>
           </div>
        <?php } ?>
       </div>
     </div>

      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>