
<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'slide-4.jpg'; ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid w-100">
  </div>

  <div class="outers_breadcrumbs_cont">
    <div class="prelatife container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb p-0">
          <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">OUR SOLUTIONS</li>
        </ol>
      </nav>
    </div>
  </div>

	<div class="inners_cover wow fadeInDown">		
    <div class="inners_cvr">
			<div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h2>OUR SOLUTIONS</h2>
            <div class="py-1"></div>
            <p>Choose the right solutions that fits your lifestyle</p>
          </div>
        </div>
			</div>
		</div>

	</div>
</section>

<section class="solution_outer_content back-white py-5">
  <div class="py-3"></div>
  <div class="prelatife container">

    <div class="outer_box_landing">
      <div class="text-center tops_title">
        <h3>TAILOR-MADE SOLUTIONS</h3>
      </div>

      <div class="py-4"></div>

       <div class="lists_thumbs">
         <div class="row">
           <div class="col-md-20">
             <div class="box_items">
               <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'water_heater')); ?>">
                <img src="<?php echo $this->assetBaseurl.'land-cate-tophead.jpg' ?>" alt="">
               <div class="infos_btm"><span>Electric Water Heater Solutions</span></div>
               </a>
               <div class="clear"></div>
             </div>
           </div>
           <div class="col-md-20">
             <div class="box_items">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'heat_pump')); ?>">
               <img src="<?php echo $this->assetBaseurl.'land-cate-airlis.jpg' ?>" alt="">
               <div class="infos_btm"><span>Domestic Heat Pump</span></div>
               </a>
               <div class="clear"></div>
             </div>
           </div>
           <div class="col-md-20">
             <div class="box_items">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/products/', 'category'=>'asesoris')); ?>">
               <img src="<?php echo $this->assetBaseurl.'land-cate-towel.jpg' ?>" alt="">
               <div class="infos_btm"><span>Towel Dryer</span></div>
               </a>
               <div class="clear"></div>
             </div>
           </div>
           
         </div>
       </div>
      <div class="clear"></div>
    </div>

    <div class="d-none d-sm-block">
          <div class="py-4 my-2"></div>
    </div>
    
    <div class="clear"></div>
  </div>
</section>
