<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['product_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['product_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['product_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="productDetail_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3 d-none d-sm-block"></div>

    <div class="content-text text-center">
      
      <h2><?php echo $data->description->name ?></h2>

      <div class="py-2"></div>
      <div class="blocks_product_ndetail_box p-4 text-left">
        <div class="inner p-2">
          <div class="row">
            <div class="col-md-23">
              <div class="pictures"><img src="<?php echo Yii::app()->baseUrl.'/images/product/'. $data->image ?>" alt="<?php echo $data->description->name ?>" class="img img-fluid w-100"></div>
            </div>
            <div class="col-md-37">
              <?php echo $data->description->desc; ?>
              <p class="mb-0"><strong>Size Variations<br></strong></p>
                <?php echo ($data->description->varian); ?>

              <p class="mb-0"><strong>Usage / Applications<br></strong></p>
                <?php echo $data->description->application; ?>
              
            </div>
          </div>
        </div>
      </div>
      <div class="py-2"></div>
      <div class="row teexts_info_bottomsn text-left">
        <div class="col">
          <a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link"><img src="<?php echo $this->assetBaseurl ?>new/icon-prevs.png" alt="" class="img img-fluid mr-2">BACK TO PRODUCT LIST</a>
        </div>
        <div class="col text-right">
          <?php if (!empty($nexts_id)): ?>
          <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $nexts_id->id, 'lang'=>Yii::app()->language)); ?>" class="btn btn-link">NEXT PRODUCT<img src="<?php echo $this->assetBaseurl ?>new/icon-nexts.png" alt="" class="img img-fluid ml-2"></a>
          <?php endif ?>
        </div>
      </div>

      <div class="py-4 my-2"></div>
      <div class="clear"></div>

      <?php 
      $n_product_data = [
                          [
                            'pict'=>'products_n_ifura_1.jpg',
                            'names'=>'BlockBoard MLH',
                          ],
                          [
                            'pict'=>'products_n_ifura_2.jpg',
                            'names'=>'BlockBoard Falcata',
                          ],
                          [
                            'pict'=>'products_n_ifura_3.jpg',
                            'names'=>'Plywood Falcata 12-23mm',
                          ],
                          [
                            'pict'=>'products_n_ifura_4.jpg',
                            'names'=>'Plywood Falcata 7.5 - 11.5mm',
                          ]
                        ];
      ?>
      <div class="outer_content_products">
        <h3>other ifra products</h3>
        <div class="py-3"></div>
        
        <div class="lists_prd_default">
          <div class="row">

            <?php foreach ($other as $key => $value): ?>
            <div class="col-md-15 col-30">
              <div class="boxed">
                <div class="picture">
                  <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'slug'=>$value->description->name, 'lang'=>Yii::app()->language)); ?>">
                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(250,234, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img img-fluid">
                  </a>
                </div>
                <div class="info">
                  <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'slug'=>$value->description->name, 'lang'=>Yii::app()->language)); ?>"><span><?php echo $value->description->name ?></span></a>
                </div>
              </div>
            </div>
            <?php endforeach ?>

          </div>
        </div>

        <div class="clear"></div>
      </div>

      <?php  ?>


      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>