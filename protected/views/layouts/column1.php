<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<?php 
    // get slides
    /*
    $criteria = new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('active = "1"');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->order = 't.urutan ASC';
    $slides = Slide::model()->findAll($criteria);
    */
?>

<div class="outer-slider-hero homepage">
    <div id="carousel_home" class="carousel slide" data-ride="carousel" data-interval="4000">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl.'slide-1.jpg' ?>" alt="First slide">
          <?php /*<div class="carousel-caption d-none d-md-block">
            <h4>COMFORT AT ALL MOMENTS</h4>
            <p>with large choice Thermor solutions</p>
            <div class="py-2 my-2"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
          </div>*/ ?>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl.'slide-2.jpg' ?>" alt="First slide">
          <?php /*<div class="carousel-caption d-none d-md-block">
            <h4>COMFORT AT ALL MOMENTS</h4>
            <p>with large choice Thermor solutions</p>
            <div class="py-2 my-2"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
          </div>*/ ?>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl.'slide-3.jpg' ?>" alt="First slide">
          <?php /*<div class="carousel-caption d-none d-md-block">
            <h4>COMFORT AT ALL MOMENTS</h4>
            <p>with large choice Thermor solutions</p>
            <div class="py-2 my-2"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
          </div>*/ ?>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl.'slide-4.jpg' ?>" alt="First slide">
          <?php /*<div class="carousel-caption d-none d-md-block">
            <h4>COMFORT AT ALL MOMENTS</h4>
            <p>with large choice Thermor solutions</p>
            <div class="py-2 my-2"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
          </div>*/ ?>
        </div>
      </div>
      <div class="carousel-caption d-none d-md-block">
        <h4>COMFORT AT ALL MOMENTS</h4>
        <p>with large choice Thermor solutions</p>
        <div class="py-2 my-2"></div>
        <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-light btns_custom_def">Learn More</a>
      </div>
    </div>
</div>

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<?php $this->endContent(); ?>
