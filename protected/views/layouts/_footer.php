<div class="tops_footetr_red py-4">
    <div class="prelatife container">
        <div class="inner">
            <div class="row">
                <div class="col-md-15 col-30 my-auto">
                    <div class="txts_icon">
                        <img src="<?php echo $this->assetBaseurl.'sft_footers_icn_1.jpg' ?>" alt="" class="img img-fluid d-inline-block mr-3">
                        <span class="d-inline-block">Engineered in France</span>
                    </div>
                </div>
                <div class="col-md-15 col-30 my-auto">
                    <div class="txts_icon">
                        <img src="<?php echo $this->assetBaseurl.'sft_footers_icn_2.jpg' ?>" alt="" class="img img-fluid d-inline-block mr-3">
                        <span class="d-inline-block">High-performance solutions</span>
                    </div>
                </div>
                <div class="col-md-15 col-30 my-auto">
                    <div class="txts_icon">
                        <img src="<?php echo $this->assetBaseurl.'sft_footers_icn_3.jpg' ?>" alt="" class="img img-fluid d-inline-block mr-3">
                        <span class="d-inline-block">Expert since 1931</span>
                    </div>
                </div>
                <div class="col-md-15 col-30 my-auto">
                    <div class="txts_icon">
                        <img src="<?php echo $this->assetBaseurl.'sft_footers_icn_4.jpg' ?>" alt="" class="img img-fluid d-inline-block mr-3">
                        <span class="d-inline-block">Worldwide presence</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="foot py-4">
    <div class="prelatife container my-2">
        <div class="inner-foot">
            <div class="py-2"></div>
            <div class="row">
                <div class="col-md-15">
                    <div class="lgo_footer"><a href="#"><img src="<?php echo $this->assetBaseurl.'lgo-footers.png' ?>" alt="" class="img-fluid"></a></div>
                </div>
                <div class="col-md-15">
                    <div class="sub-footer">
                        <h6 class="sub_title">THERMOR BRAND</h6>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>#our-story">Our Story</a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>#our-value">Our Values</a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>#our-product">Our Company</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="sub-footer">
                        <h6 class="sub_title"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">THEMOR WATER HEATERS</a></h6>
                        <?php 
                        $data_cat = DataProducts::resources();
                        ?>
                        <ul class="list-unstyled">
                            <?php foreach ($data_cat as $k_key => $k_val): ?>
                                <?php foreach ($k_val as $key => $value): ?>
                                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']), 'category'=>$k_key )); ?>">Thermor <?php echo ucwords(strtolower($value['title']) ); ?></a></li>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="sub-footer">
                        <h6 class="sub_title">STAY CONNECTED</h6>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">Our Partner</a></li>
                            <!-- <li><a href="#">Our online store</a></li> -->
                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>