
<section class="about-gallery-back py-5">
  <div class="prelatife container">
    <div class="inners pt-4 my-2">
      <div class="lists_others_prdDetail">
        <?php 
        $other_all = array_slice($other_all, 0, 4);
        ?>
        <div class="row">
          <?php foreach ($other_all as $key => $value): ?>
            <div class="col-md-15">
              <div class="items">
                <div class="picture">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']) )); ?>">
                    <img src="<?php echo $this->assetBaseurl .'product/'. $value['folder'].'/'. $value['picture'][0] ?>" alt="" class="img-fluid">
                  </a>
                </div>
                <div class="info text-center py-3">
                  <h5><a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'slug'=> Slug::Create($value['title']) )); ?>"><?php echo 'Thermor '. ucwords(strtolower($value['title'])). '<br>Water Heater' ?></a></h5>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
      <?php /*
      <!-- start jquery owl -->
      <div class="owl-carousel owl-theme lists_gallery_def">
        <?php for ($i=1; $i < 12; $i++) { ?>
          <div class="item">
            <div class="bx-item">
              <img src="<?php echo $this->assetBaseurl.'galls-about-1.jpg' ?>" alt="" class="img img-fluid">
            </div>  
          </div>
        <?php } ?>
      </div>
      <!-- 1end jquery owl -->
      */ ?>

      <div class="clear"></div>
    </div>
  </div>
</section>

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      dots:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
  })
</script> -->