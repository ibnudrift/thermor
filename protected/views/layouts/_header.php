<?php 
$active_menu_pg = $this->id.'/'.$this->action->id;
?>
<header class="head py-4">
  <div class="prelatife container d-none d-sm-block">
    <div class="lgo-headers"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo-head-thermor.jpg" alt="" class="img img-fluid"></a></div>
    <div class="clear"></div>
    <div class="py-2"></div>
    <div class="row">
      <div class="col-md-45">
        <div class="top-menu">
          <ul class="list-inline m-0">
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/products_landing')); ?>">Our Solutions</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">Our Partner</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3"></div>
      <div class="col-md-12">
        <div class="box-searchs">
          <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">
            <label class="sr-only">Search</label>
            <input type="text" name="q" class="form-control" placeholder="Search..." value="<?php echo isset($_GET['q'])? $_GET['q'] : ''; ?>">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>

  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo-head-thermor.jpg" alt="" class="img img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/products_landing')); ?>">Our solutions</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About us</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">Our Partner</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact us</a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div><div class="height-2"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-20 col-sm-20">
        <div class="clear height-5"></div>
        <div class="lgo-web-aofku">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo-head-thermor.jpg" alt="" style="max-width: 165px;">
          </a>
        </div>
      </div>
      <div class="col-md-40 col-sm-40">
        <div class="text-right">
          <div class="py-2 my-1"></div>
          <div class="menu-taffix">
            <ul class="list-inline m-0">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/products_landing')); ?>">Our Solutions</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">Our Partner</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>