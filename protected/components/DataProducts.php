<?php
/*
 * sort order
 * By Deory Pandu
 */
class DataProducts {

    public static function resources($names=array())
    {
        
        $heat_power = [
                    // Ristretto hz
                    /*
                    [
                        'folder'=>'ristretto',
                        'picture' => ['big-pic-ristritto-sld-1.jpg', 'big-pic-ristritto-sld-2.jpg'],
                        'title' => 'RISTRETTO HZ',
                        'intro_desc'=> 'Pemanas Air Horizontal, solusi untuk ruang terbatas dan atas plafon',
                        'description' => '<p><strong>JAMINAN KENYAMANAN</strong></p><ul><li>Panel kontrol untuk suhu</li><li>Koneksi samping untuk hemat ruang</li></ul><p><strong>HEMAT ENERGI</strong></p><ul><li>Thermostat mekanik dengan keamanan terintegrasi</li><li>Inlet Air efisiensi tinggi</li><li>Insulasi kepadatan tinggi bebas CFC untuk penghematan energi lebih banyak</li></ul>',
                        'additional_icon' => [],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',
                        
                        'why_list_data' => [
                                            [
                                                'icons'=>'BIG - PC - RISTRTTO  HZ - ICON- 01.jpg',
                                                'text'=>'Tipe Instalasi',
                                            ],
                                            [
                                                'icons'=>'BIG - PC - RISTRTTO  HZ - ICON- 02.jpg',
                                                'text'=>'Ketahanan Tabung',
                                            ],
                                            [
                                                'icons'=>'BIG - PC - RISTRTTO  HZ - ICON- 03.jpg',
                                                'text'=>'Technology O`Pro',
                                            ],
                                            [
                                                'icons'=>'BIG - PC - RISTRTTO  HZ - ICON- 04.jpg',
                                                'text'=>'Elemen Pemanas',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi O`Pro',
                        'benefit_content' => '<p><strong>KUALITAS - DURABILITAS</strong></p><ul><li>Sistem anti korosi, menambah ketahanan tabnung dan elemen hingga 50%</li><li>Magnesium Anoda untuk memperkuat perlindungan tangki</li><li>Enamel berkualitas generasi baru (tangki berlapis kaca)</li><li>Katup pelepas tekanan</li><li>Dielectric union</li><li>Lip Gasket khusus untuk menghindari korosi di sekitar flens</li></ul><ul><li><strong>USER-FRIENDLY</strong></li><li>IP24 - kepatuhan penuh dengan standar Eropa untuk keamanan listrik dan perlindungan pengguna</li><li>Kabel ELCB untuk keamanan pengguna</li><li>Indikator lampu pemanasan</li></ul>',
                        'spec_pictures' => 'bg-pic-RISTRTTO-HZ.png',
                        'spec_content' => '<ul><li>Enamel Zirkonium kualitas berlian</li><li>Pipa outlet terbuat dari stainless steel</li><li>Magnesium Anoda</li><li>Elemen pamanas lapisan tembaga</li><li>Inlet difuser</li><li>Teknologi O`Pro</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Kenop kontrol bawah</li></ul>',
                        'data_dimensi_instalasi' => 'big-pic-ristritto-sld-2.jpg',
                        'data_teknikal' => 'capacity-table.jpg',
                        'data_dimensi' => 'dimensi-table.jpg',
                    ],
                    
                    // Concept
                    [
                        'folder'=>'concept',
                        'picture' => ['big-pc-concept-sld-1.jpg', 'big-pc-concept-sld-2.jpg', 'big-pc-concept-sld-3.jpg'],
                        'title' => 'CONCEPT',
                        'intro_desc'=> 'Pemanas Air Klasik mengandalkan keunggulan Thermor',
                        'description' => '<p><strong>JAMINAN KENYAMANAN</strong></p><ul><li>Termometer  krom (50L - 100L)</li><li>Desain Perancis</li></ul><p><strong>HEMAT ENERGI</strong></p><ul><li>Inlet Air efisiensi tinggi</li><li>Insulasi kepadatan tinggi bebas CFC untuk penghematan energi lebih banyak</li></ul>',
                        'additional_icon' => [],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',
                        
                        'why_list_data' => [
                                            [
                                                'icons'=>'pc-concept-icon-1.jpg',
                                                'text'=>'Tipe Instalasi',
                                            ],
                                            [
                                                'icons'=>'pc-concept-icon-2.jpg',
                                                'text'=>'Ketahanan Tabung',
                                            ],
                                            [
                                                'icons'=>'pc-concept-icon-3.jpg',
                                                'text'=>'Technology O`Pro',
                                            ],
                                            [
                                                'icons'=>'pc-concept-icon-4.jpg',
                                                'text'=>'Elemen Pemanas',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi O`Pro',
                        'benefit_content' => '<p><strong>KUALITAS - DURABILITAS</strong></p><ul><li>Thermostat mekanik dengan keamanan terintegrasi</li><li>Magnesium Anoda untuk memperkuat perlindungan tangki</li><li>Enamel berkualitas berlian generasi baru (tangi berlapis kaca)</li><li>Katup pelepas tekanan</li><li>Dielectric union</li><li>Lip Gasket khusus untuk menghindari korosi di sekitar flens</li></ul><p><strong>USER-FRIENDLY</strong></p><ul><li>IP24 - kepatuhan penuh dengan standar Eropa untuk keamanan listrik dan perlindungan pengguna</li><li>Kabel ELCB untuk keamanan pengguna</li><li>Indikator lampu pemanasan</li></ul>',
                        'spec_pictures' => 'bg-pic-RISTRTTO-HZ.png',
                        'spec_content' => '<ul><li>Enamel Zirkonium kualitas berlian</li><li>Pipa outlet terbuat dari stainless steel</li><li>Magnesium Anoda</li><li>Elemen pamanas lapisan tembaga</li><li>Inlet difuser</li><li>Teknologi O`Pro</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Kenop kontrol bawah</li></ul>',
                        'data_dimensi_instalasi' => 'big-pc-concept-sld-2.jpg',
                        'data_teknikal' => 'capacity-table.jpg',
                        'data_dimensi' => 'dimensi-table.jpg',
                    ],*/

                    // Steatite
                    [
                        'folder'=>'steatite',
                        'picture' => ['bg-pic-seattle.jpg', 'bg-pic-seattle_2.jpg', 'bg-pic-seattle_3.jpg', 'bg-pic-seattle_4.jpg'],
                        'title' => 'STEATITE',
                        'intro_desc'=> 'Pemanas Air dengan Teknologi Elemen Pemanas Keramik kering untuk penggunaan tahan lama',
                        'description' => '<ul><li><strong>Thermor, brand Spesialis International</strong> di bidang pemanas air</li><li><strong>Keamanan</strong> Dilengkapi dengan Thermostat, Safety Cut Out dan ELCB</li><li><strong>Enamel Zirkonium berkualitas berlian</strong> untuk kekuatan dan ketahanan tabung terhadap korosi yang disebabkan oleh basa, asam, air garam, dan lainnya</li><li>Multiposisi: dapat dipasang secara horizontal maupun vertikal (kapasitas 75L dan 100L)</li><li><strong>Lebih hemat energi hingga 20%</strong> dengan insulasi kepadatan tinggi</li><li><strong>IP2 4 Rating berstandard Internasional</strong> untuk keamanan listrik dan perlindungan konsumen (waterproof protection)</li></ul>',
                        'additional_icon' => [],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',
                        
                        'why_list_data' => [
                                            [
                                                'icons'=>'icons_seatle_1.png',
                                                'text'=>'Thermor, brand Spesialis International di bidang pemanas air',
                                            ],
                                            [
                                                'icons'=>'icons_seatle_2.png',
                                                'text'=>'Keamanan Dilengkapi dengan Thermostat, Safety Cut Out dan ELCB',
                                            ],
                                            [
                                                'icons'=>'icons_seatle_3.png',
                                                'text'=>'Enamel Zirkonium berkualitas berlian untuk kekuatan dan ketahanan tabung',
                                            ],
                                            [
                                                'icons'=>'icons_seatle_4.png',
                                                'text'=>'P24 Rating berstandard Internasional untuk keamanan listrik dan perlindungan konsumen (waterproof protection)',
                                            ],
                                            [
                                                'icons'=>'icons_seatle_5.png',
                                                'text'=>'Multiposisi: dapat dipasang secara horizontal maupun vertikal (kapasitas 75L dan 100L)',
                                            ],
                                            [
                                                'icons'=>'icons_seatle_6.png',
                                                'text'=>'Lebih hemat energi hingga 20% dengan insulasi kepadatan tinggi',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi Steatite',
                        'benefit_content' => '<ul><li><strong>Thermor, brand Spesialis International</strong> di bidang pemanas air</li><li><strong>Keamanan</strong> Dilengkapi dengan Thermostat, Safety Cut Out dan ELCB</li><li><strong>Enamel Zirkonium berkualitas berlian</strong> untuk kekuatan dan ketahanan tabung terhadap korosi yang disebabkan oleh basa, asam, air garam, dan lainnya</li><li>Multiposisi: dapat dipasang secara horizontal maupun vertikal (kapasitas 75L dan 100L)</li><li><strong>Lebih hemat energi hingga 20%</strong> dengan insulasi kepadatan tinggi</li><li><strong>IP2 4 Rating berstandard Internasional</strong> untuk keamanan listrik dan perlindungan konsumen (waterproof protection)</li></ul>',
                        'spec_pictures' => 'bgn-pics_seattles.jpg',
                        'spec_content' => '<ol><li>Tabung enamel Zirkonium berkualitas berlian</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Magnesium anoda; perlindungan dari korosi</li><li>Elemen pemanas “Steatite”</li><li>Inlet diffuser</li><li>Pipa outlet terbuat dari stainless steel</li><li>Tombol pengaturan</li></ol>',
                        'data_dimensi_instalasi' => 'STEATITE.svg',
                        'data_teknikal' => '<table><thead><tr><th rowspan="2"> <strong>Kapasitas (Liter)</strong></th><th colspan="8"><strong>Dimensi (mm)</strong></th><th rowspan="2"><strong>Flange opening (Ø mm)</strong></th></tr></thead><tbody><tr><td><strong>A</strong></td><td><strong>B</strong></td><td><strong>C</strong></td><td><strong>D</strong></td><td><strong>E</strong></td><td><strong>F</strong></td><td><strong>G</strong></td><td><strong>J</strong></td></tr><tr><td><strong>30</strong></td><td><strong>624</strong></td><td><strong>342</strong></td><td><strong>387</strong></td><td><strong>100</strong></td><td><strong>100</strong></td><td><strong>184</strong></td><td><strong>1/2’’</strong></td><td><strong>-</strong></td><td><strong>76</strong></td></tr><tr><td><strong>50</strong></td><td><strong>909</strong></td><td><strong>342</strong></td><td><strong>387</strong></td><td><strong>100</strong></td><td><strong>100</strong></td><td><strong>184</strong></td><td><strong>1/2’’</strong></td><td><strong>-</strong></td><td><strong>76</strong></td></tr><tr><td><strong>75</strong></td><td><strong>712</strong></td><td><strong>490</strong></td><td><strong>532</strong></td><td><strong>163</strong></td><td><strong>230</strong></td><td><strong>350/440</strong></td><td><strong>3/4’’</strong></td><td><strong>-</strong></td><td><strong>76</strong></td></tr><tr><td><strong>100</strong></td><td><strong>883</strong></td><td><strong>490</strong></td><td><strong>532</strong></td><td><strong>163</strong></td><td><strong>230</strong></td><td><strong>350/440</strong></td><td><strong>3/4’’</strong></td><td><strong>-</strong></td><td><strong>76</strong></td></tr></tbody></table>',
                        'data_dimensi' => '<table><thead><tr><th> <strong>Kapasitas (Liter)</strong></th><th> <strong>Daya Listrik (Watt)</strong></th><th> <strong>Voltase (Volt)</strong></th><th> <strong>Berat Netto (Kg)</strong></th><th> <strong>Tekanan Maksimal (Bar)</strong></th><th> <strong>Konsumsi Energi (KWH/24h)</strong></th><th> <strong>Estimasi Kebutuhan</strong></th></tr></thead><tbody><tr><td> <strong>30</strong></td><td> 500</td><td> 220-240</td><td> 14.5</td><td> 8</td><td> 0.63</td><td> 1 shower (2-3 Orang)</td></tr><tr><td> <strong>50</strong></td><td> 750</td><td> 220-240</td><td> 19.5</td><td> 8</td><td> 0.86</td><td> 2 shower (4 orang) / 1 bathtub</td></tr><tr><td> <strong>75</strong></td><td> 750</td><td> 220-240</td><td> 26</td><td> 8</td><td> 0.91</td><td> 3 shower (6 orang) / 1 bathtub</td></tr><tr><td> <strong>100</strong></td><td> 1200</td><td> 220-240</td><td> 31</td><td> 8</td><td> 1.08</td><td> 4 shower (8 orang) / 2 bathtub</td></tr></tbody></table>',
                        'manual' => 'manual-steatite.pdf',
                    ],

                    // Compact Access
                    [
                        'folder' => 'compact_access',
                        'picture' => ['bg-pic-access.jpg', 'bg-pic-access_2.jpg', 'bg-pic-access_3.jpg', 'bg-pic-access_4.jpg'],
                        'title' => 'COMPACT ACCESS',
                        'intro_desc'=> 'Pemanas Air Modern dengan perangkat yang mudah digunakan',
                        'description' => 'Pemanas Air Modern dengan perangkat yang mudah digunakan',
                        'additional_icon' => [
                                                [
                                                    'icons'=>'fn_prds_icon_2.jpg',
                                                    'text'=>'Lampu indikator menunjukkan produksi air panas',
                                                ],
                                                [
                                                    'icons'=>'fn_prds_icon_3.jpg',
                                                    'text'=>'Pemanas air modern dengan perangkat yang mudah digunakan',
                                                ],
                                            ],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],                                      

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',

                        'why_list_data' => [
                                            [
                                                'icons'=>'icons_seatle_1.png',
                                                'text'=>'Thermor, brand Spesialis International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_2.jpg',
                                                'text'=>'Keamanan dilengkapi dengan Thermostat',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_3.jpg',
                                                'text'=>'Enamel Zirkonium berkualitas berlian',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_4.jpg',
                                                'text'=>'IP24 Rating berstandard International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_5.jpg',
                                                'text'=>'Lebih hemat energi 20%',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_6.jpg',
                                                'text'=>'Elemen pemanas Tembaga',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi O’Pro',
                        'benefit_content' => '<p>Sistem anti korosi, menambah ketahanan tabung dan elemen hingga 50%</p>
                                            <p>O’Pro (Ohmic Resistor) adalah perangkat anti korosi dengan sistem listrik pasif. Sistem ini menyeimbangkan reaksi listrik antara tabung dan elemen pemanas. Teknologi ini meningkatkan ketahanan magnesium anoda dan perlindungan terhadap korosi. </p>
                                            <p>Magnesium anoda terletak di bagian tengah tabung dan menyediakan perlindungan terbaik terhadap korosi.</p>',

                        'spec_pictures' => 'bgn-pcs-access.jpg',
                        'spec_content' => '<ol><li>Teknologi O’Pro</li><li>Tabung dengan enamel Zirkonium berkualitas berlian</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Elemen pemanas terbuat dari tembaga, tahan korosi</li><li>Perangkat Ergonomi</li><li>Desain compact memudahkan penempatan di space terbatas</li><li>Inlet diffuser</li><li>Pipa outlet terbuat dari stainless steel</li><li>Magnesium Anoda</li></ol>',
                        'data_dimensi_instalasi' => 'COMPACT ACCESS.svg',
                        'data_teknikal' => '<table><thead><tr><th> <strong>Kapasitas (Liter)</strong></th><th> <strong>Daya Listrik (Watt)</strong></th><th> <strong>Voltase (Volt)</strong></th><th> <strong>Berat Netto (Kg)</strong></th><th> <strong>Tekanan Maksimal (Bar)</strong></th><th> <strong>Konsumsi Energi (KWH/24h)</strong></th><th> <strong>Estimasi Kebutuhan</strong></th></tr></thead><tbody><tr><td> 15</td><td> 350</td><td> 220-240</td><td> 8.1</td><td> 8</td><td> 0.46</td><td> 1 shower (1 Orang)</td></tr><tr><td> 30</td><td> 500</td><td> 220-240</td><td> 11.7</td><td> 8</td><td> 0.46</td><td> 1 shower (2-3 Orang)</td></tr></tbody></table>',
                        'data_dimensi' => '<table><thead><tr><th rowspan="2"><strong>Kapasitas (Liter)</strong></th><th rowspan="2"><strong>Koneksi Intel/Outlet</strong></th><th colspan="6"><strong>Dimensi (mm)</strong></th><th rowspan="2"><strong>Flange opening (Ø mm)</strong></th></tr><tr><th><strong>A</strong></th><th><strong>B</strong></th><th><strong>C</strong></th><th><strong>D</strong></th><th><strong>E</strong></th><th><strong>F</strong></th></tr></thead><tbody><tr><td><strong>15</strong></td><td>1/2’’</td><td>396</td><td>367</td><td>324</td><td>98</td><td>100</td><td>62</td><td></td></tr><tr><td><strong>30</strong></td><td>1/2’’</td><td>473</td><td>446</td><td>407</td><td>115</td><td>100</td><td>62</td><td></td></tr></tbody></table>',
                        'manual' => 'manual-compact.pdf',
                    ],

                    // Compact Evo
                    [
                        'folder' => 'compact_evo',
                        'picture' => ['bg-pic-evo.jpg', 'bg-pic-evo_2.jpg', 'bg-pic-evo_3.jpg', 'bg-pic-evo_4.jpg'],
                        'title' => 'COMPACT EVO',
                        'intro_desc'=> 'Keindahan dari desain yang unik dan modern',
                        'description' => '',
                        'additional_icon' => [
                                                [
                                                    'icons'=>'fn_prds_icon_a1.jpg',
                                                    'text'=>'Desain yang unik dan modern cocok untuk kebutuhan setiap rumah',
                                                ],
                                                [
                                                    'icons'=>'fn_prds_icon_ncamera.jpg',
                                                    'text'=>'User Friendly interface dengan 2 lampu indikator siap pemanasan dan siap pakai',
                                                ],
                                            ],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',

                        'why_list_data' => [
                                            [
                                                'icons'=>'icons_seatle_1.png',
                                                'text'=>'Thermor, brand Spesialis International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_2.jpg',
                                                'text'=>'Keamanan dilengkapi dengan Thermostat',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_3.jpg',
                                                'text'=>'Enamel Zirkonium berkualitas berlian',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_4.jpg',
                                                'text'=>'IP24 Rating berstandard International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_5.jpg',
                                                'text'=>'Lebih hemat energi 20%',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_6.jpg',
                                                'text'=>'Elemen pemanas Tembaga',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi O’Pro',
                        'benefit_content' => '<p>Sistem anti korosi, menambah ketahanan tabung dan elemen hingga 50%</p>
                                            <p>O’Pro (Ohmic Resistor) adalah perangkat anti korosi dengan sistem listrik pasif. Sistem ini menyeimbangkan reaksi listrik antara tabung dan elemen pemanas. Teknologi ini meningkatkan ketahanan magnesium anoda dan perlindungan terhadap korosi. </p>
                                            <p>Magnesium anoda terletak di bagian tengah tabung dan menyediakan perlindungan terbaik terhadap korosi.</p>',

                        'spec_pictures' => 'bgn-pcs-evo.jpg',
                        'spec_content' => '<ol><li>Teknologi O’Pro</li><li>Tabung dengan enamel Zirkonium berkualitas berlian</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Elemen pemanas terbuat dari tembaga, tahan korosi</li><li>Perangkat Ergonomi</li><li>Desain compact memudahkan penempatan di space terbatas</li><li>Inlet diffuser</li><li>Pipa outlet terbuat dari stainless steel</li><li>Magnesium Anoda</li></ol>',

                        'data_dimensi_instalasi' => 'COMPACT EVO.svg',
                        'data_teknikal' => '<table><thead><tr><th> <strong>Kapasitas (Liter)</strong></th><th> <strong>Daya Listrik (Watt)</strong></th><th> <strong>Voltase (Volt)</strong></th><th> <strong>Berat Netto (Kg)</strong></th><th> <strong>Tekanan Maksimal (Bar)</strong></th><th> <strong>Konsumsi Energi (KWH/24h)</strong></th><th> <strong>Estimasi Kebutuhan</strong></th></tr></thead><tbody><tr><td> 15</td><td> 350</td><td> 220-240</td><td> 8.1</td><td> 8</td><td> 0.5</td><td> 1 shower (1 Orang)</td></tr><tr><td> 30</td><td> 500</td><td> 220-240</td><td> 11.7</td><td> 8</td><td> 0.7</td><td> 1 shower (2-3 Orang)</td></tr></tbody></table>',
                        'data_dimensi' => '<table><thead><tr><th rowspan="2"><strong>Kapasitas (Liter)</strong></th><th rowspan="2"><strong>Koneksi Intel/Outlet</strong></th><th colspan="5"><strong>Dimensi (mm)</strong></th><th rowspan="2"><strong>Flange opening (Ø mm)</strong></th></tr><tr><th><strong>A</strong></th><th><strong>B</strong></th><th><strong>C</strong></th><th><strong>D</strong></th><th><strong>E</strong></th></tr></thead><tbody><tr><td><strong>15</strong></td><td>1/2’’</td><td>396</td><td>367</td><td>324</td><td>98</td><td>100</td><td></td></tr><tr><td><strong>30</strong></td><td>1/2’’</td><td>473</td><td>446</td><td>407</td><td>115</td><td>100</td><td></td></tr></tbody></table>',
                        'manual' => 'manual-compact.pdf',
                    ],

                    // Compact HZ
                    [
                        'folder' => 'compact_hz',
                        'picture' => ['bg-pic-HZ.jpg', 'bg-pic-HZ_2.jpg', 'bg-pic-HZ_3.jpg', 'bg-pic-HZ_4.jpg'],
                        'title' => 'COMPACT HZ',
                        'intro_desc'=> 'Desain Ultra-Slim dan Modern',
                        'description' => '',
                        'additional_icon' => [
                                                [
                                                    'icons'=>'fn_prds_icon_Hz1.jpg',
                                                    'text'=>'Magnesium Anoda Ganda untuk perlindungan anti korosi tambahan',
                                                ],
                                                [
                                                    'icons'=>'fn_prds_icon_Hz2.jpg',
                                                    'text'=>'Desain Horizontal Ramping cocok untuk kebutuhan setiap rumah',
                                                ],
                                                [
                                                    'icons'=>'fn_prds_icon_ncamera.jpg',
                                                    'text'=>'User Friendly Interface dengan 2 lampu indikator siap pemanasan dan siap pakai',
                                                ],
                                            ],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',

                        'why_list_data' => [
                                            [
                                                'icons'=>'icons_seatle_1.png',
                                                'text'=>'Thermor, brand Spesialis International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_2.jpg',
                                                'text'=>'Keamanan dilengkapi dengan Thermostat',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_3.jpg',
                                                'text'=>'Enamel Zirkonium berkualitas berlian',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_4.jpg',
                                                'text'=>'IP24 Rating berstandard International',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_5.jpg',
                                                'text'=>'Lebih hemat energi 20%',
                                            ],
                                            [
                                                'icons'=>'fn_prds_icon_6.jpg',
                                                'text'=>'Elemen pemanas Tembaga',
                                            ],
                                           ],
                        'benefit_content_title' => 'Teknologi O’Pro',
                        'benefit_content' => '<p>Sistem anti korosi, menambah ketahanan tabung dan elemen hingga 50%</p>
                                            <p>O’Pro (Ohmic Resistor) adalah perangkat anti korosi dengan sistem listrik pasif. Sistem ini menyeimbangkan reaksi listrik antara tabung dan elemen pemanas. Teknologi ini meningkatkan ketahanan magnesium anoda dan perlindungan terhadap korosi. </p>
                                            <p>Magnesium anoda terletak di bagian tengah tabung dan menyediakan perlindungan terbaik terhadap korosi.</p>',

                        'spec_pictures' => 'bgn-pics_com_hz.jpg',
                        'spec_content' => '<ol><li>Teknologi O’Pro</li><li>Tabung dengan enamel Zirkonium berkualitas berlian</li><li>Polyurethane bebas 0% CFC dengan kepadatan tinggi</li><li>Elemen pemanas terbuat dari tembaga, tahan korosi</li><li>Perangkat Ergonomi</li><li>Desain compact memudahkan penempatan di space terbatas</li><li>Inlet diffuser</li><li>Pipa outlet terbuat dari stainless steel</li><li>Magnesium Anoda</li></ol>',

                        'data_dimensi_instalasi' => 'COMPACT HZ.svg',
                        'data_teknikal' => '<table><thead><tr><td> <strong>Kapasitas (Liter)</strong></td><td> <strong>Daya Listrik (Watt)</strong></td><td> <strong>Voltase (Volt)</strong></td><td> <strong>Berat Netto (Kg)</strong></td><td> <strong>Tekanan Maksimal (Bar)</strong></td><td> <strong>Konsumsi Energi (KWH/24h)</strong></td><td> <strong>Estimasi Kebutuhan</strong></td></tr></thead><tbody><tr><td> <strong>20</strong></td><td> 350</td><td> 220-240</td><td> 11</td><td> 8</td><td> 0.55</td><td> 1 shower (1 Orang)</td></tr><tr><td> <strong>30</strong></td><td> 500</td><td> 220-240</td><td> 13</td><td> 8</td><td> 0.77</td><td> 1 shower (2-3 Orang)</td></tr></tbody></table>',
                        'data_dimensi' => '<table><thead><tr><th rowspan="2"><strong>Kapasitas (Liter)</strong></th><th rowspan="2"><strong>Koneksi Intel/Outlet</strong></th><th colspan="10"><strong>Dimensi (mm)</strong></th><th rowspan="2"><strong>Flange opening (Ø mm)</strong></th></tr><tr><th><strong>A</strong></th><th><strong>B</strong></th><th><strong>C</strong></th><th><strong>D</strong></th><th><strong>E</strong></th><th><strong>F</strong></th><th><strong>G</strong></th><th><strong>H</strong></th><th><strong>I</strong></th><th><strong>J</strong></th></tr></thead><tbody><tr><td><strong>20</strong></td><td>1/2’’</td><td>600</td><td>320</td><td>355</td><td>355</td><td>160</td><td>190</td><td>260</td><td>90</td><td>250</td><td>100</td><td>78</td></tr><tr><td><strong>30</strong></td><td>1/2’’</td><td>810</td><td>320</td><td>355</td><td>355</td><td>160</td><td>190</td><td>340</td><td>90</td><td>355</td><td>100</td><td>78</td></tr></tbody></table>',
                        'manual' => 'manual-compact.pdf',
                    ],

                ];

        $asesoris = [
                     // Towel
                    [
                        'folder'=>'towel',
                        'picture' => ['bg-pc-towel-01.jpg', 'bg-pc-towel-02.jpg', 'bg-pc-towel-03.jpg'],
                        'title' => 'CORSAIRE TOWEL DRYER',
                        'intro_desc'=> 'Solusi untuk Pengering Handuk',
                        'description' => '<p><strong>JAMINAN KENYAMANAN</strong></p><ul><li>Tersedia model Slim dan Curve&nbsp;</li><li>Tombol pengatur fungsi dan temperatur</li><li>Desain putih elegan</li></ul>',
                        'additional_icon' => [],
                        'cvr_icon' => [
                                        [
                                          'name'=>'MODELS',
                                          'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                        ],
                                        [
                                          'name'=>'OUTPUT',
                                          'values'=>'<p>350 to 500 W</p>',
                                        ],
                                        [
                                          'name'=>'HEATING ELEMENT',
                                          'values'=>'<p>Copper Immersion heating element</p>
                                                    <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                        ],
                                      ],

                        'file_technical_sheet' => '',
                        'file_user_manual' => '',
                        
                        'why_list_data' => [],
                        'benefit_content_title' => '',
                        'benefit_content' => '',

                        'spec_pictures' => 'bg-pc-towel-diag-nn.png',
                        'spec_content' => '<ul><li>Tombol pengatur fungsi</li><li>Lampu turbo</li><li>Lampu indikator pemanasan</li><li>Tombol pengatur temperatur</li></ul>',
                        'data_dimensi_instalasi' => 'bg-pc-towel-03.jpg',
                        'data_teknikal' => 'capacity-table.jpg',
                        'data_dimensi' => '',
                        'manual' => '',
                    ],
                ];

        $heat_pump = [
                        // Airlis
                        [
                            'folder'=>'airlis',
                            'picture' => ['bg-pc-nairlis-01.jpg', 'bg-pc-nairlis-02.jpg'],
                            'title' => 'AIRLIS',
                            'intro_desc'=> 'Pilihan Pemanas Air Heat Pump Hemat Biaya',
                            'description' => '<p><strong>JAMINAN KENYAMANAN</strong></p><ul><li>Tidak bising - cocok untuk rumah tangga</li><li>Nyaman berkat cadangan listrik (elemen pemanas Steatite)</li><li>Koneksi resirkulasi untuk memberikan kenyamanan semua penggunaan</li></ul><p><strong>HEMAT ENERGI</strong></p><ul><li>Penghematan energi hingga 65%</li><li>Inlet Air efisiensi tinggi</li><li>Insulasi kepadatan tinggi bebas CFC untuk penghematan energi lebih banyak</li></ul>',
                            'additional_icon' => [],
                            'cvr_icon' => [
                                            [
                                              'name'=>'MODELS',
                                              'values'=>'<img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pedal.jpg'.'" class="img img-fluid">',
                                            ],
                                            [
                                              'name'=>'OUTPUT',
                                              'values'=>'<p>350 to 500 W</p>',
                                            ],
                                            [
                                              'name'=>'HEATING ELEMENT',
                                              'values'=>'<p>Copper Immersion heating element</p>
                                                        <img src="'.Yii::app()->baseUrl.'/asset/images/'.'icons_service_md_pro.jpg'.'" class="img img-fluid">',
                                            ],
                                          ],

                            'file_technical_sheet' => '',
                            'file_user_manual' => '',
                            
                            'why_list_data' => [
                                                [
                                                    'icons'=>'bg-pc-nairlis-icon-1.jpg',
                                                    'text'=>'Tipe Instalasi',
                                                ],
                                                [
                                                    'icons'=>'bg-pc-nairlis-icon-2.jpg',
                                                    'text'=>'Hemat Energi',
                                                ],
                                                [
                                                    'icons'=>'bg-pc-nairlis-icon-3.jpg',
                                                    'text'=>'Teknologi ACi Hybrid',
                                                ],
                                                [
                                                    'icons'=>'bg-pc-nairlis-icon-4.jpg',
                                                    'text'=>'Elemen Pemanas',
                                                ],
                                               ],
                            'benefit_content_title' => 'Teknologi ACi Hybrid',
                            'benefit_content' => '<p><strong>KUALITAS - DURABILITAS</strong></p><ul><li>ACI hybrid perlindungan anti-korosi</li><li>Teknologi Steatite: Elemen pemanas keramik kering</li><li>Enamel berkualitas berlian generasi baru (tangki berlapis kaca)</li><li>Katup pelepas tekanan</li><li>Dielectric Union</li><li>Lip Gasket khusus untuk menghindari korosi di sekitar flens</li></ul><p><strong>USER-FRIENDLY</strong></p><ul><li>Ringkas dan pemasangan mudah</li><li>Panel kontrol intuitif</li><li>Instalasi: Udara sekitar</li></ul>',
                            'spec_pictures' => 'bg-pc-airliss-diag-nn.png',
                            'spec_content' => '<ul><li>Heat Pump efisiensi tinggi dengan rentang penggunaan suhu yang luas</li><li>Panel kontrol intuitif</li><li>Enamel Zirkonium kualitas berlian</li><li>Teknologi Steatite: Elemen pemanas keramik kering</li><li>Inlet/Outlet Udara</li><li>ACI hybrid sistem anti korosif dengan arus paksa dan anoda magnesium</li><li>Eco Mode: disarankan untuk mengoptimalkan penghematan energi tanpa mengurangi kenyamanan</li><li>Absence Mode: menghasilkan penghematan energi saat pengguna tidak di rumah</li><li>Kenop kontrol suhu</li></ul>',
                            'data_dimensi_instalasi' => 'bg-pc-nairlis-02.jpg',
                            'data_teknikal' => 'capacity-table.jpg',
                            'data_dimensi' => 'dimensi-table.jpg',
                            'manual' => '',
                        ],
                    ];

        $results = [
                    'water_heater'=>$heat_power,
                    'heat_pump'=> $heat_pump,
                    'asesoris'=> $asesoris,
                   ];

        return $results;        
    }


}
