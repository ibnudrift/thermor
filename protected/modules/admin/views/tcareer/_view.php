<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_en')); ?>:</b>
	<?php echo CHtml::encode($data->desc_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_id')); ?>:</b>
	<?php echo CHtml::encode($data->desc_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kualifikasi_en')); ?>:</b>
	<?php echo CHtml::encode($data->kualifikasi_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kualifikasi_id')); ?>:</b>
	<?php echo CHtml::encode($data->kualifikasi_id); ?>
	<br />


</div>