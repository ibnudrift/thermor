<?php

/**
 * This is the model class for table "store_address".
 *
 * The followings are the available columns in table 'store_address':
 * @property string $id
 * @property string $nama_toko
 * @property string $phone
 * @property string $address
 * @property string $status
 * @property string $kota
 */
class StoreAddress extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StoreAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_toko', 'length', 'max'=>31),
			array('phone', 'length', 'max'=>36),
			array('status', 'length', 'max'=>5),
			array('kota', 'length', 'max'=>225),
			array('address', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_toko, phone, address, status, kota', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_toko' => 'Nama Toko',
			'phone' => 'Phone',
			'address' => 'Address',
			'status' => 'Status',
			'kota' => 'Kota',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama_toko',$this->nama_toko,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('kota',$this->kota,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}